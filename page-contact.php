<?
	// Template Name: Contact
?>
<?get_header()?>

<main>

	<?php if(have_posts()) : while (have_posts()) : the_post();?>

		<article class="content">
			<section class="contact-content">
				<div class="contact-form">
					<h2>Email Us</h2>
					<?php the_content(); ?>
				</div>

				<div class="office-info">
					<h2>Location</h2>

					<?php if(have_rows('locations', 'option')): ?>
						<?php while(have_rows('locations', 'option')): the_row();
							$name = get_sub_field('location_name');
							$street = get_sub_field('street');
							$suite = get_sub_field('suite');
							$city = get_sub_field('city');
							$state = get_sub_field('state');
							$zip = get_sub_field('zip');
							$phone = get_sub_field('phone');
							$tel = str_replace(array('.', ' ', ',', '-', '(', ')'), '' , $phone);
							$map = get_sub_field('map');
							$gmb = get_sub_field('gmb');
							?>

							<div class="gmaps"><?php echo $map; ?></div>
							<div class="loc-hours">
								<div class="contact-location">
									<div class="practice"><strong><?php echo $name; ?></strong></div>
									<a href="<?php echo $gmb; ?>" target="blank" rel="nofollow">
										<div class="street"><?php the_sub_field('street'); ?><?php if( $suite ): ?>, <?php echo $suite; ?><?php endif; ?></div>
										<div class="city"><?php echo $city; ?>, <?php echo $state; ?> <?php echo $zip; ?></div>
									</a>
									<div class="phone"><a href="tel:1<?php echo $tel; ?>"><?php echo $phone; ?></a></div>
								</div>

								<?php if( have_rows('hours', 'option') ): ?>
									<div class="hours-repeater">
										<strong>Office Hours</strong>
										<?php while( have_rows('hours', 'option') ): the_row();
											$day = get_sub_field('day');
											$hours = get_sub_field('hours');
											?>

											<?php if(!empty($day) ): ?>
												<div class="hours-wrap">
													<div class="day"><?php echo $day; ?></div>
													<div class="hours"><?php echo $hours; ?></div>
												</div>
											<?php endif; ?>

										<?php endwhile; ?>
									</div>
								<?php endif; ?>

							</div>

						<?php endwhile; ?>
					<?php endif; ?>

				</div>

			</section>
		</article>
	<?php endwhile; endif;?>
</main>

<?get_footer()?>