<?
	// Template Name: Home
?>

<?get_header()?>

	<div class="welcome">
		<h2><?php the_field('welcome_title'); ?></h2>
		<h3><?php the_field('welcome_subtitle'); ?></h3>
		<a href="#doctor"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
	</div>

	<section class="doctor" id="doctor">
		<h2><?php the_field('doctor_title'); ?></h2>
		<h3><?php the_field('doctor_subtitle'); ?></h3>
		<div class="description">
			<div class="docimg"><img src="<?php bloginfo('template_directory'); ?>/images/img-doctor.png" alt=" Steven Turkeltaub, MD, PC"></div>
			<div class="describe">
				<span><?php the_field('doctor_callout'); ?></span>
				<?php the_field('doctor_text'); ?>
			</div>
		</div>
		<div class="buttons">
			<?php if(have_rows('doctor_buttons')): ?>
				<?php while(have_rows('doctor_buttons')): the_row(); ?>
					<a href="<?php the_sub_field('link'); ?>" class="button" rel="nofollow"><?php the_sub_field('text'); ?></a>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</section>

	<section class="home-reviews">
		<h2><?php the_field('reviews_title'); ?></h2>
		<?php if(have_rows('reviews')): ?>
			<ul>
				<?php while(have_rows('reviews')): the_row(); ?>
					<li>
						<?php the_sub_field('review'); ?>
					</li>
				<?php endwhile; ?>
			</ul>
		<?php endif; ?>
		<div class="seeall">
			<a href="<?php the_field('reviews_link'); ?>"> 		
				<i class="fa fa-star" aria-hidden="true"></i>
				<i class="fa fa-star" aria-hidden="true"></i>
				<i class="fa fa-star" aria-hidden="true"></i>
				<i class="fa fa-star" aria-hidden="true"></i>
				<i class="fa fa-star" aria-hidden="true"></i>
				<span>See All Patient Reviews</span>
			</a>
		</div>
	</section>

	<div class="procedures">
		<h2><?php the_field('procedure_title'); ?></h2>
		<div class="boxes">
			<?php if(have_rows('procedures')): ?>
				<?php while(have_rows('procedures')): the_row(); ?>
					<a href="<?php the_sub_field('link'); ?>" class="<?php the_sub_field('name'); ?>" style="background-image:url(<?php the_sub_field('image'); ?>);">
						<div class="a-procedure" > 
							<span><?php the_sub_field('name'); ?></span>
						</div>
					</a>	
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
		<a href="<?php the_field('gallery_button_link'); ?>" class="gallery-button"><?php inline_svg('icon-gallery'); ?> <?php the_field('gallery_button_text'); ?></a>
	</div>

<?get_footer()?>