<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content=" maximum-scale=1.0, user-scalable=0, width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<title><?php wp_title(""); ?></title>

	<script> /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)?(document.getElementsByTagName("html")[0].className+=" is--device",/iPad/i.test(navigator.userAgent)&&(document.getElementsByTagName("html")[0].className+=" is--ipad")):document.getElementsByTagName("html")[0].className+=" not--device";</script>

	<?php if(!is_404()): ?>
		<!-- Google Fonts -->
		<?php miniCSS::url( 'https://fonts.googleapis.com/css?family=Montserrat:400,700' ); ?>
		<?php miniCSS::url( 'https://fonts.googleapis.com/css?family=Julius+Sans+One' ); ?>

	<?php endif; ?>

	<!-- wp loads all css -->
	<?php wp_head()?>

	<!-- Setting up Google Analytics -->
	<?php $google_analytics = $value = get_field( "google_analytics", 'option' );  ?>

	<?php if(!empty($google_analytics)): ?>
		<?php echo $google_analytics; ?>
	<?php else: ?>

	<?php endif; ?>

</head>

<?php bodyClass(); ?>

<a href="#skiptomaincontent" style="display:none;">Skip to main content</a>

<header class="site-header <?php echo is_front_page() ? 'front-header' : 'int-header'; ?>">

	<div class="nav-bar">
		<div class="menu-buttons">
			<div class="menu-trigger">
				<span class="touch-button-icon"><?php inline_svg('icon-menu'); ?></span>
				<span class="touch-button-text">Menu</span>
			</div>
			<nav>
				<?php wp_nav_menu( array(
					'menu' 		=> 'Main',
					'container_class' => 'menu-wrap',
					'menu_id'	=> 'menu-main',
					'menu_class' => 'main-menu',
					)); ?>
			</nav> 
			<div class="menu-contact">
				<div class="menu-phone">
					<?php
					$phone_number = get_field('phone', 'options');
					$tel_number = str_replace(array('.', ',', '-', '(', ')'), '' , $phone_number);
					?>

					<a href="tel:1<?php echo $tel_number; ?>">
						<span class="touch-button-icon"><?php inline_svg('icon-phone'); ?></span>
						<span class="touch-button-text"><?php echo $phone_number; ?></span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<!--
	<div class="masthead">	
		
		<div class="logo">
			 <h1><a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/logo.png" alt="Plastic Surgeon Steven Turkeltaub, MD"></a></h1>
		</div>
		<div class="locations-grip">
			<?php if(have_rows('locations','options')): ?>
				<?php while(have_rows('locations','options')): the_row(); ?>
					<div class="header-locations">
						<a href="<?php the_sub_field('address_map_link', 'option'); ?>" target="_blank">
							<span><?php the_sub_field('short_name', 'option'); ?></span>
						</a>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>
			<div class="mast-phone"><a href="tel:1<?php echo $tel_number; ?>">
				<span class=""><?php echo $phone_number; ?></span>
			</a>
			</div>
			<div class="schedule">
				<a href="<?php bloginfo('url'); ?>/contact-us/">Schedule Consultation  <?php inline_svg('icon-schedule'); ?></a>
			</div>  
		</div>
	</div> 
	-->



	<!-- H1 Structure 

	<?php if(is_front_page()): ?>

		<?php // do nothing if homepage  ?>		

	<?php elseif(this_is('gallery-case')): ?>
		<?php $category_title =  get_the_title($post->in_cat_ID); ?>
		<section class="page-title gallery-title">
			<h1><?php echo $category_title ?></h1>
		</section>
		<section class="site-crumbs">
			<?php echo __salaciouscrumb(); ?>
		</section>

	<?php elseif(this_is('gallery')): ?>
		<section class="page-title gallery-title">
			<h1>Patient Photo Gallery</h1>
		</section>
		<section class="site-crumbs">
			<?php echo __salaciouscrumb(); ?>
		</section>

	<?php elseif (is_home() or is_search() or is_archive()): ?>
		<section class="page-title">
			<h1>Our Blog<?php if (is_archive()):  ?> : <?the_time('Y')?><? endif;  ?></h1> 
		</section>

	<?php else: ?> 
		<section class="page-title">
			<h1><?the_title();?></h1>
		</section>
		<section class="site-crumbs">
			<?php echo __salaciouscrumb(); ?>
		</section>
		
	<?php endif; ?>
	
	-->

</header>


