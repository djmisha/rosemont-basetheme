<?php

if( !defined('TMPL_DIR')):
	define('TMPL_DIR' , get_template_directory() );
endif;
if( !defined('TMPL_DIR_URI')):
	define('TMPL_DIR_URI' , get_template_directory_uri() );
endif;

/**
*	rm-functions.php must be included before anything else
**/
include TMPL_DIR . '/inc/functions.php';

/* NAV WALKER */
if(file_exists(TMPL_DIR . '/inc/walkers/walkerpagecustom.php')):
	include TMPL_DIR . '/inc/walkers/walkerpagecustom.php';
endif;


/* Activate after going live for Site Speed / SEO */
// include 'inc/optimize/modifiedheaders.php';


/**
* THEME SETUPS
*/
function __themesetup(){
	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	//add_theme_support( 'title-tag' );
	/**
	 * Add thumbnail functionality
	 */
	add_theme_support('post-thumbnails');

}
add_action( 'after_setup_theme', '__themesetup' , 2 );

/**
* CSS
*/
function __themecss(){

	wp_register_style( 'owl' , TMPL_DIR_URI . '/js/libs/owl-carousel/assets/owl.carousel.css');
	wp_register_style( 'fancybox' , TMPL_DIR_URI . '/js/libs/fancybox3/jquery.fancybox.css');
	wp_register_style( 'fontawesome' , TMPL_DIR_URI . '/fonts/fontawesome/css/font-awesome.css' );
	wp_register_style( 'rm-theme' , get_stylesheet_uri() , array('owl' , 'fancybox' , 'fontawesome') , '1' );

	wp_enqueue_style( 'rm-theme' );

}
add_action('wp_enqueue_scripts','__themecss');

/**
* JAVASCRIPTS
*/
function __themejs(){
	global $wp_scripts;
	//Required
	wp_deregister_script('jquery');
	wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js", false, "2.1.3", true);
	wp_register_script( 'modernizr', TMPL_DIR_URI . '/js/libs/modernizr.min.js', false, '2.8.3', false );
	//Optional
	wp_register_script('rm-cookie', TMPL_DIR_URI . '/js/libs/jquery.cookie.min.js', array('jquery','modernizr'), '1.0', true );
	// wp_register_script('rm-blazy', TMPL_DIR_URI . '/js/libs/blazy/blazy.js', array('jquery','modernizr'), '1.0', true );
	// wp_register_script('rm-blazy-polyfill', TMPL_DIR_URI . '/js/libs/blazy/polyfills/closest.js', array('jquery','modernizr'), '1.0', true );
	wp_register_script('rm-fancybox', TMPL_DIR_URI . '/js/libs/fancybox3/jquery.fancybox.min.js', array('jquery','modernizr'), '1.0', true );
	// wp_register_script('rm-match', TMPL_DIR_URI . '/js/libs/match-height/jquery.matchHeight-min.js', array('jquery','modernizr'), '1.0', true );
	// wp_register_script('rm-owl', TMPL_DIR_URI . '/js/libs/owl-carousel/owl.carousel.js', array('jquery','modernizr'), '1.0', true );
	// wp_register_script('rm-parallax', TMPL_DIR_URI . '/js/libs/parallax/jquery.parallax.js', array('jquery','modernizr'), '1.0', true );
	// wp_register_script('rm-waypoints', TMPL_DIR_URI . '/js/libs/parallax/jquery.waypoints.min.js', array('jquery','modernizr'), '1.0', true );
	// wp_register_script('rm-wow', TMPL_DIR_URI . '/js/libs/wow/wow.min.js', array('jquery','modernizr'), '1.0', true );
	// wp_register_script('rm-harvey', TMPL_DIR_URI . '/js/libs/harvey.min.js', array('jquery','modernizr'), '1.0', true );
	//RM Scripts
	wp_register_script('rm-menu', TMPL_DIR_URI . '/js/rm-menu.js', array('jquery','modernizr'), '1.0', true );
	wp_register_script('theme-js', TMPL_DIR_URI . '/js/scripts.js', array('jquery','modernizr'), '1.0', true );


	$data_array = rm_data_array();
	wp_localize_script( $handle = 'theme-js', $object_name = 'rm_data', $l10n = $data_array ); //found in rm-functions.php
	wp_enqueue_script( 'rm_js_data' );


	//Enqueue All Scripts
	//Enqueue Required
	wp_enqueue_script( 'jquery');
	wp_enqueue_script( 'rm_modernizr');
	//Enqueue Optional
	wp_enqueue_script( 'rm-cookie');
	// wp_enqueue_script( 'rm-blazy');
	// wp_enqueue_script( 'rm-blazy-polyfill');
	wp_enqueue_script( 'rm-fancybox');
	// wp_enqueue_script( 'rm-match');
	// wp_enqueue_script( 'rm-owl');
	// wp_enqueue_script( 'rm-parallax');
	// wp_enqueue_script( 'rm-waypoints');
	// wp_enqueue_script( 'rm-wow');
	// wp_enqueue_script( 'rm-harvey');
	//Enqueue RM Scripts
	wp_enqueue_script( 'rm-menu');
	wp_enqueue_script( 'theme-js');
}
add_action('wp_enqueue_scripts','__themejs');

/*******************************************
		Activated only when going live
*******************************************/


// add_filter('inline/css' , function($tag = null ,$handle = null ,$src = null){

// 	$newtag = '';
// 	if($_SERVER['HTTP_HOST'] != 'rosemontdev.com'):
// 		/*
// 			RUN ONLY ON LIVE (not dev)
// 		*/

// 		$templatepath = get_template_directory_uri();
// 		if(in_array($handle , array('rm-theme' , 'fancybox' , 'owl' , 'fontawesome')))://list the styles you want to target
// 			$templatepath2 = preg_replace('/https?:\/\//i' , '' , $templatepath);
// 			$src2 = preg_replace('/https?:\/\/|\?(.*)/i','',$src);
// 			$src2 = str_replace("$templatepath2", '' , $src2 );
// 			$newtag = miniCSS::file( $src2 , array('echo'=>false));
// 		endif;

// 	endif;
// 	if(!empty($newtag)): return $newtag; endif;
// 	return $tag;
// },1,3);

/* Defer Modernir Load for Site Speed */

// add_filter('script_loader_tag', 'add_defer_attribute', 11, 2);
// function add_defer_attribute($tag, $handle) {
//     if ( 'modernizr' !== $handle )
//         return $tag;
//     return str_replace( ' src', ' defer="defer" src', $tag );
// }

/*******************************************
			Your Custom Code Below
*******************************************/


// Global ACF Options

$siteName = get_bloginfo('name') . ' Settings';

if( function_exists('acf_add_options_page') ) {

	$page = acf_add_options_page(array(
		'page_title' 	=> $siteName,
		'menu_title' 	=> $siteName,
		'menu_slug' 	=> 'theme-general-settings',
		'capability' 	=> 'edit_posts',
		'redirect' 	=> false,
		'icon_url' => 'dashicons-admin-customizer'
	));

}

// Inline SVG

function inline_svg($name) {
  $file = get_template_directory();
  $file .= "/images/svg/" . $name . ".svg";
  include($file);
}

// Is Page or Parent

function is_tree($pid) {
	global $post;
	if(is_page()&&($post->post_parent==$pid||is_page($pid)))
		return true;
	else
		return false;
};


//custom excerpt length

function my_excerpt($excerpt_length = 55, $id = false, $echo = true) {

    $text = '';

	  if($id) {
	  	$the_post = & get_post( $my_id = $id );
	  	$text = ($the_post->post_excerpt) ? $the_post->post_excerpt : $the_post->post_content;
	  } else {
	  	global $post;
	  	$text = ($post->post_excerpt) ? $post->post_excerpt : get_the_content('');
    }

		$text = strip_shortcodes( $text );
		$text = apply_filters('the_content', $text);
		$text = str_replace(']]>', ']]&gt;', $text);
	  $text = strip_tags($text);

		$excerpt_more = '' . '... <span>Read More</span>';
		$words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
		if ( count($words) > $excerpt_length ) {
			array_pop($words);
			$text = implode(' ', $words);
			$text = $text . $excerpt_more;
		} else {
			$text = implode(' ', $words);
		}
	if($echo)
  echo apply_filters('the_content', $text);
	else
	return $text;
}

function get_my_excerpt($excerpt_length = 55, $id = false, $echo = false) {
 return my_excerpt($excerpt_length, $id, $echo);
}

// [sitemap omit="1051,432"]
function sitemap_function( $atts ){
ob_start();  ?>

  <ul>
  <?php wp_list_pages(
    array(
     'title_li' => '',
     'exclude' => $atts['omit'],
     'depth' => $atts['depth']
   ) ); ?>
  </ul>
<?php
$sitemap = ob_get_clean();
return $sitemap;
}
add_shortcode( 'sitemap', 'sitemap_function' );