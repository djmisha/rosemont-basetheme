<?get_header()?>

<section class="search-bar">
	<div class="cat-select">
		<div class="cat-options">Categories <i class="fa fa-angle-down" aria-hidden="true"></i></div>
		<ul class="cats">
			<?php
			$postCategories = get_categories(array('parent'=> 0));
			$postLink = get_category_link( $postCategory->term_id );

			foreach($postCategories as $postCategory): ?>


			<li>
				<a href="<?php bloginfo('url'); ?>/category/<?php echo $postCategory->slug; ?>/">
					<?php echo $postCategory->name; ?>
				</a>
			</li>
		<?php endforeach; ?>
	</ul>
</div>

<div class="cat-select">
	<div class="cat-options">Archives <i class="fa fa-angle-down" aria-hidden="true"></i></div>
	<ul class="list-items cats">
		<?php wp_get_archives( array(
			'type'            => 'yearly',
			'limit'           => '',
			'before'          => '',
			'after'           => '',
			'show_post_count' => false,
			'echo'            => 1,
			'order'           => 'DESC'
			)); ?>
		</ul>
	</div>

	<?php $cats = get_the_category(); global $wp_query;	?>

	<div class="search-form">
		<form action="<?php bloginfo('url'); ?>" id="search-form" method="get"><input type="text" placeholder="" name="s" id="s" class="search-input"><input type="submit" value="Search" class="button">
		</form>
	</div>

</section>
<main class="interior">

	<article class="content">

		
		<?if ( have_posts() ) : while ( have_posts() ) : the_post();?>

			<article class="post-snippet">

				<div class="excerpt">
					<h2 class="blog-title"><a href="<?the_permalink();?>"><?the_title();?></a></h2>
					
					
					<?php if(!empty(get_the_post_thumbnail())): ?>
						<div class="thumb">
							<a href="<?php the_permalink(); ?>">
								<?php the_post_thumbnail('thumbnail'); ?>
							</a>
						</div>
					<?php endif; ?>

					<div class="meta-data">Posted on <?the_time('M');?> <?the_time('j');?>, <? the_time('Y'); ?> in <?php the_category(', '); ?></div>
					<div class="para">
						<a href="<?php the_permalink(); ?>">
							<?php my_excerpt(40); ?>
						</a>
						
					</div>
					<!-- <a href="<?php the_permalink(); ?>" class="more button">Read More</a> -->
				</div>

			</article>
			<?endwhile; endif;?>

			<?php posts_nav_link(); ?>

		</article>

		<?get_sidebar()?>
	</main>

	<?get_footer()?>