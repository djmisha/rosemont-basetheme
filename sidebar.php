<aside>

	<!-- Sidebar Buttons --> 

	<div class="sidebar-block sidebar-buttons">
		<?php if(have_rows('buttons_block', 'option')): ?>
			<ul>
				<?php while(have_rows('buttons_block', 'option')): the_row(); ?>
					<li>
						<?php the_sub_field(''); ?>

						<a class="button" href="<?php the_sub_field('button_link'); ?>"><span><?php the_sub_field('button_text'); ?></span></a>
					</li>
				<?php endwhile; ?>
			</ul>
		<?php endif; ?>
	</div>

	<!-- Sidebar Related Pages -->

	<?php if(!is_page(array('contact-us'))): ?>
		<?php if(!this_is('gallery-child' || 'gallery') && get_post_type() != 'post' && get_post_type() != 'news-room'):
			global $post;
			$related_id = ( $post->post_parent ) ? $post->post_parent : $post->ID;
			$childPages = wp_list_pages(array(
					'title_li'  	=> '',
					'child_of'  	=> $post->ID,
					'depth'   	=> 1,
					'echo'		=> 0
			));
			$wp__list_pages = wp_list_pages(array(
					'title_li'  	=> '',
					'child_of'  	=> $related_id,
					'exclude'		=> "$post->ID",
					'depth'   	=> 1,
					'echo'		=> 0
			));
			if(!empty($childPages)):
				$wp__list_pages = $childPages;
			endif;
			if( !empty($wp__list_pages) ): ?>
				
				<div class="sidebar-block related-pages">
					<div><h4>Related Pages</h4></div>
					<div>
						<ul class="list-unstyled">
							<?php echo $wp__list_pages; ?>
						</ul>
					</div>
				</div>

			<?php endif; ?>
		<?php endif; ?>
	<?php endif; ?>
	 

	

<?wp_reset_postdata(); ?>

</aside>
