<?php get_header()?>

<!-- <section class="page-title">
  <h1><?//the_title();?></h1>
  <?php //echo __salaciouscrumb(); ?>
</section>
 -->
<main class="interior">

<?php if(have_posts()) : while (have_posts()) : the_post();?>
	<article class="content" id="skiptomaincontent">

		<?php the_content(); ?>

		<!-- Related Posts on Pages -->

        <?php
            global $post;
            $custom_fields = get_post_custom($post->ID);
            $my_custom_field = $custom_fields['relatedMetaBox'];

            if (isset($my_custom_field )):
                foreach ( $my_custom_field as $key => $value )
                if (isset($my_custom_field)) {
                    echo "<h2 class='related-hdng'>Related Posts</h2>";
                }
                $args = array(
                    'post_type' => 'post',
                    'category__and' => array( $value ),
                    'posts_per_page' => 3
                );
                // the query
                $the_query = new WP_Query( $args );
                ?>
                <div class="related-articles">
                <?php if ( $the_query->have_posts() ) : ?>
                    <!-- the loop -->
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                          <div class="rm-postContent inline-block">
                          <?php the_post_thumbnail('thumbnail'); ?>
                          <a href="<?php //the_permalink(); ?>"><?php the_title(); ?></a>
                          <!-- <a href="<?php //the_permalink(); ?>">Read More</a> -->

                          </div>

                    <?php endwhile; ?>
                    <!-- end of the loop -->
                    <?php wp_reset_postdata(); ?>
                <?php else: endif; ?>
             	</div>

            <?php endif; ?>

		<?php edit_post_link( $link = __('<< EDIT >>'), $before = "<p>", $after ="</p>", $id ); ?>

	</article>
<?php endwhile; endif;?>

<?php get_sidebar()?>
</main>

<?php get_footer()?>