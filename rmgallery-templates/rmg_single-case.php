<? get_header();?>


<section class="single-case-content">
	<div class="back-btn">
		
		<a href="<?php bloginfo('url'); ?>/gallery"><i class="fa fa-angle-left" aria-hidden="true"></i> Back to Gallery</a>

		<div class="gallery-nav">
		<?php
		/**
		* accepts an array
		* keys : 'class' , 'title' , 'hash'
		* if 'title' key is not present then default will be
		* - prev : &laquo; Previous
		* - next : Next &raquo;
		*/

		$rmg_case::prev( array(
		    'class' => 'button-rmg button-gallery-prev' ,
		    'title'  => '<i class="fa fa-angle-left" aria-hidden="true"></i> <span>Prev</span> ') );

		$rmg_case::next( array(
		    'class' => 'button-rmg button-gallery-next' ,
		    'title' => ' <span>Next</span> <i class="fa fa-angle-right" aria-hidden="true"></i>') );
		?>
		</div>
	</div>

<?php $category_title =  get_the_title($post->in_cat_ID); ?>

	<section class="case-wrap">
		<span class="case-title">&nbsp;<?php the_title(); ?></span>

			<?if ( have_posts() ) : while ( have_posts() ) : the_post();?>
				<div class="img-wrap">
				<?php foreach ( $post->rmg_case_imgs as $img ): ?>
					<div class="before-img img-frame">
						<img class="first" src="<?php echo $rmg_case::get_image($img['before_image_path']); ?>" alt="" />
						<div class="bna-label">Before</div>
					</div>
					<div class="after-img img-frame">
						<img src="<?php echo $rmg_case::get_image($img['after_image_path']); ?>" alt="" />
						<div class="bna-label">After</div>
					</div>

				<?php endforeach; ?>
				</div>
				<?php if(!empty(the_content())): ?>
				<div class="details-hdng">Details:</div>
				<div class="patient-details"><?the_content();?></div>
				<?php endif; ?>
			<?endwhile; endif;?>

	</section>

	<section class="gallery-buttons">
			<a class="button" href="<?php bloginfo('url'); ?>/about-us/practice/"><span>About Us</span></a>
			<a class="button" href="<?php bloginfo('url'); ?>/patient-reviews/"><span>Patient Reviews</span></a>
			<a class="button" href="<?php bloginfo('url'); ?>/blog/"><span>Our Blog</span></a>
			<a class="button" href="<?php bloginfo('url'); ?>/contact-us/"><span>Contact Us</span></a>
	</section>
</section>




<? get_footer();?>