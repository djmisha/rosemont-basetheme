<!--
	<?php if(!is_page(array('contact-us'))): ?>
		<section class="quick-contact">
			<div class="label">Request an Appointment</div>
			<div class="sticky-form"><?php echo do_shortcode('[seaforms name="quick-contact"]'); ?></div>
		</section>
	<?php endif; ?>

	<?php if(!is_home()): ?>
		<?php if( get_post_type() != 'post'): ?>
		<section class="recent-blogs">
			<h3>Featured Blog Posts</h3>
			<div class="blogs">
				<?php
					$args = array(
						'numberposts' => 2,
						'post_status'=>"publish",
						'post_type'=>"post",
						'orderby'=>"post_date");
					$postslist = get_posts( $args );
					echo '<ul class="latest-posts">';
					foreach ($postslist as $post) :  setup_postdata($post); ?>
						<li>
							<a href="<?the_permalink();?>">
								<div class="blog-teaser">
									<div class="thumb"><?php the_post_thumbnail('thumbnail', array( 'alt' => trim(strip_tags( $post->post_title )),)); ?></div>
									<div>
										<div class="hdng"><?the_title();?></div>
										<div class="excerpt"><?php my_excerpt(); ?></div>	
									</div>
								</div>
							</a>
						</li>
					<?php endforeach; ?>
					<?php wp_reset_query(); ?>
					</ul>
			</div>
		</section>
		<?php endif; ?>
	<?php endif; ?>
-->
	<footer class="site-footer">
		<section class="practice-info">
			<div class="logo-phones">
				<div class="logo">
					<a href="<?php bloginfo('url'); ?>">
						<!-- <img src="<?php bloginfo('template_directory'); ?>/images/logo-footer.png"> -->
					</a>
				</div>
			<!--	<div class="phones">
					<?php
						$phone_number = get_field('phone', 'options');
						$tel_number = str_replace(array('.', ',', '-', '(', ')'), '' , $phone_number);
					?>
					<div><span>Phone</span> <a href="tel:1<?php echo $tel_number; ?>"><?php the_field('phone', 'option'); ?></a></div>
					<div><span>Fax</span> <?php the_field('fax', 'option'); ?></div>
				</div>
			-->
			</div>
			<div class="memberships">
				<?php if(have_rows('memeberships', 'option')): ?>
					<ul>
						<?php while(have_rows('memeberships', 'option')): the_row(); ?>
							<li>
								<img src="<?php the_sub_field('logo', 'option'); ?>" alt="Membership logo">
							</li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
			</div>
		</section>
		<section class="practice-locations">
			<?php if(have_rows('locations','options')): ?>
				<?php while(have_rows('locations','options')): the_row(); ?>
					<div class="footer-locations">
						<a href="<?php the_sub_field('address_map_link', 'option'); ?>" target="_blank">
							<div class="location">
								<img src="<?php the_sub_field('icon', 'option'); ?>" alt="icon">
								<strong><?php the_sub_field('address_name', 'option'); ?></strong><br>
								<?php the_sub_field('address_street', 'option'); ?><br>
								<?php the_sub_field('address_suite', 'option'); ?><br>
								
								<?php the_sub_field('address_city', 'option'); ?>,
								<?php the_sub_field('address_state', 'option'); ?>,
								<?php the_sub_field('address_zip', 'option'); ?><br>	
								<span>Map &amp; Directions</span>	
							</div>	
						</a>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>
		</section>
		<div class="social">
			<a href="<?php the_field('facebook','options'); ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
			<a href="<?php the_field('google_plus','options'); ?>" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
			<a href="<?php the_field('twitter','options'); ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
			<!-- <a href="<?php the_field('youtube','options'); ?>" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a> -->
		</div>
		<section class="footer-nav">
			<?php wp_nav_menu(array('menu' => 'Main'));?>
		</section>
		<!-- <div class="azbreast">Please visit our BREAST SPECIALTY SITE for more information: <a href="https://www.arizonabreast.com/" target="_blank">www.arizonabreast.com</a></div> -->
		<section class="lower-footer">
			<div class="copyright">Copyright &copy; <?=date("Y")?> <?bloginfo('title');?>. All rights reserved | <a href="<?php bloginfo('url'); ?>/privacy-policy">Privacy Policy</a> | <a href="<?php bloginfo('url'); ?>/sitemap/" title="Sitemap">Sitemap</a></div>
			<div class="rm-sig"><a href="https://www.rosemontmedia.com/website/design-medical-specialties/" target="_blank">Medical Website Design</a> by <a href="http://www.rosemontmedia.com/" target="_blank">Rosemont Media<?php inline_svg('rm-logo'); ?></a></div>
		</section>
	</footer>

	<!-- Enqueued JavaScript at the bottom for fast page loading -->
	
	<?wp_footer();?>

	<?php
		// if($_SERVER['SERVER_NAME'] == 'rosemontdev.com'):
			$bsPort 					= 35730;
			$browserSync 			= 'http://rosemontdev.com:'.$bsPort;
			$browserSyncHdrs 		= @get_headers($browserSync);
			if($browserSyncHdrs):
				?>
				<script async src="http://rosemontdev.com:<?php echo $bsPort?>/browser-sync/browser-sync-client.js?v=2.18.8"></script>
				<?
			endif;
		// endif;
	?>

</body>
</html>