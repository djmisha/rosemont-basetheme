(function($) {
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		$('html').addClass('is--device');
		if(/iPad/i.test(navigator.userAgent)){
			$('html').addClass('is--ipad');
		}
	}
	else{
		$('html').addClass('not--device');
	}

	$(function() {//doc.ready[shorthand] start


		var $desktop = 1040;
		var $tablet = 768;
		var theme_path = rm_data.tmplDirUri;
		var site_path = rm_data.siteUrl;


		/* --------------------------------------------------
			Sticky Contact Form
		-------------------------------------------------- */

		var stickyContactTop = 400;

		var stickyContact = function(){
			var scrollTop = $(window).scrollTop();

			if (scrollTop > stickyContactTop) {
				$('.quick-contact').addClass('fixed');
			} else {
				$('.quick-contact').removeClass('fixed');
			}
		};

		stickyContact();

		$(window).scroll(function() {
			stickyContact();
		});


		/* --------------------------------------------------
			Gallery Move Elements Around 
		-------------------------------------------------- */

		$('.button-gallery-prev').insertBefore('.gallery-title h1');
		$('.button-gallery-next').insertAfter('.gallery-title h1');
		$('.case-title').appendTo('.gallery-title h1');
		

		/* --------------------------------------------------
			Smooth Anchor Scrolling
		-------------------------------------------------- */

		$('a[href*=#]:not([href=#])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					if($('body.home').length > 0){
						var padding = 0;
					}
					else{
						var padding = 0;
					}
					$('html,body').animate({
						scrollTop: target.offset().top - padding
					}, 1000);
					return false;
				}
			}
		});
		var target = location.hash;
		if (target.length > 0) {
			if($('body.home').length > 0){
				var padding = 0;
			}
			else{
				var padding = 0;
			}
			setTimeout(function() { $('html,body').animate({
				scrollTop: $(target).offset().top - padding
			}, 10);}, 100);
		}


		/* --------------------------------------------------
			Fancybox
		-------------------------------------------------- */

		$( ".gallery-icon a" ).attr( "data-fancybox", "gallery" );


		/* --------------------------------------------------
			HEY THERE POPUP
		-------------------------------------------------- */
			 
		if( $('body.rmgallery-child').length ){
			if( $.cookie('gallerypop') == null ) {
					console.log(theme_path);
					$.fancybox.open({

						src: theme_path +'/popup.php',
						type: 'ajax',
						opts: {
							scrolling : 'no',
							transitionEffect : 'fade',
							modal : true,
							helpers : {
								overlay : {
									css : {
										'background' : 'rgba(0, 0, 0, 0.96)'
									}
								}
							},
							live : true,
							afterClose : function(){
								$.cookie('gallerypop', 'rmg', { expires: 1, path: '/' });
							}
						}
							
						});
			}//end cookies check
		}

		/* --------------------------------------------------
			Blog Categories dropdown
		-------------------------------------------------- */

		$('.cat-options').on('click', function(event) {
			event.preventDefault();
			/* Act on the event */
			$(this).next().slideToggle();
			console.log('click');
		});


		/* --------------------------------------------------
			Specials Page Autofill Form Message Field
		-------------------------------------------------- */
		
		$('.specials-button').click(function() {
		    console.log('running');
		    specialsReq = $(this).data('special');
		    $('#message-5').val("I'm interested in the " + $(this).data('special') + " special.");
		});

	});// end of doc.ready
})(jQuery);